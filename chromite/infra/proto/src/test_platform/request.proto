// Copyright 2019 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

syntax = "proto3";

package test_platform;

import "chromiumos/common.proto";
import "google/protobuf/duration.proto";

option go_package = "go.chromium.org/chromiumos/infra/proto/go/test_platform";

// Request represents a request to the ChromeOS Test Platform to run a test,
// set of tests, suite of tests, or test plan on a set of devices (eventually
// also: virtual devices) in the ChromeOS Test Lab.
message Request {
  // Params defines parameters that apply to an entire Request.
  message Params {
    // HardwareAttributes defines the hardware-bound characteristics
    // of the devices to run tests on.
    message HardwareAttributes {
      // Model is the model to run tests against.
      //
      // TODO(akeshet): Re-use device protos instead of defining this
      // independently.
      string model = 1;

      // TODO(akeshet): Add other schedulable characteristics, based on shared
      // hardware config protos.
    }

    HardwareAttributes hardware_attributes = 1;

    // SoftwareAttributes defines software characteristics that are strongly
    // associated with a device, though not immutable or hardware-bound.
    message SoftwareAttributes {
      chromiumos.BuildTarget build_target = 2;

      reserved 1;
      reserved "board";

      // TODO(akeshet): add other attributes such as CTS version,
      // kernel version(?), etc.
    }

    SoftwareAttributes software_attributes = 2;

    // SoftwareDependency defines mutable software characteristics of the
    // devices to run tests on (for instance, the desired version of ChromeOS).
    message SoftwareDependency {
      reserved 1, 2;
      reserved "type", "url";

      oneof dep {
        // ChromeOS build name, e.g. "reef-release/R77-12345.0.0"
        string chromeos_build = 3;
      }
    }

    repeated SoftwareDependency software_dependencies = 3;

    // Scheduling defines parameters that affect how the tests in this
    // request are prioritized relative to other requests, and how capacity for
    // them is allocated.
    message Scheduling {
      // ManagedPool enumerates the different lab platform-managed pools.
      // Devices in these pools are automatically managed by lab platform
      // systems and can not be arbitrarily assigned to pools by users.
      //
      // Managed pool implementations are specific to test platform's scheduling
      // backends.
      // For Skylab, see
      // https://chromium.googlesource.com/infra/infra/+/5566668428c6b286702bf440d9dfd0be7bca1d84/go/src/infra/libs/skylab/inventory/device.proto#173
      // For autotest, see
      // https://chromium.googlesource.com/chromiumos/third_party/autotest/+/7436c2b6e6bd32ed1f1bd08cf8e0feb40cfe7b89/server/constants.py#18
      enum ManagedPool {
        MANAGED_POOL_UNSPECIFIED = 0;
        MANAGED_POOL_CQ = 1;
        MANAGED_POOL_BVT = 2;
        MANAGED_POOL_SUITES = 3;
        MANAGED_POOL_CTS = 4;
        MANAGED_POOL_CTS_PERBUILD = 5;
        MANAGED_POOL_CONTINUOUS = 6;
        MANAGED_POOL_ARC_PRESUBMIT = 7;
        MANAGED_POOL_QUOTA = 8;
      }

      oneof pool {
        // Managed pool of devices to run tests in.
        ManagedPool managed_pool = 1;

        // Unmanaged pool of devices to run tests in.
        // Must not be a managed pool.
        string unmanaged_pool = 2;

        // Quota account to use. If specified, request will run in the
        // quota-shared pool, and scheduled via quotascheduler.
        string quota_account = 3;
      }
    }

    Scheduling scheduling = 4;

    // Retry defines parameters that affect how failed tests within
    // a request are retried.
    message Retry {
      // TODO(akeshet): Add parameters.
    }

    Retry retry = 5;

    // Metadata defines parameters that determine where test definitions
    // and metadata are stored and discovered from.
    message Metadata {
      string test_metadata_url = 1;
    }

    Metadata metadata = 6;

    // Time defines parameters related to timeouts.
    message Time {
      // Maximum duration for the entire request to be handled.
      google.protobuf.Duration maximum_duration = 1;
    }

    Time time = 7;
  }

  Params params = 1;

  // SuiteRequest defines an individual named suite.
  message Suite {
    string name = 1;
  }

  message Test {
    enum Harness {
      HARNESS_UNSPECIFIED = 0;
      HARNESS_AUTOTEST = 1;
      // TODO(akeshet): add TAST, CTS, ...
    }

    Harness harness = 1;

    string name = 2;

    repeated string test_args = 3;
  }

  message TestPlan {
    repeated Suite suite = 1;
    repeated Test test = 2;
  }

  TestPlan test_plan = 5;


  reserved 2, 3, 4;
  reserved "suites", "tests", "test_plans";
}
