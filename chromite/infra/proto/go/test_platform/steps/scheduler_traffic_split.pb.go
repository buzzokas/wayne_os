// Code generated by protoc-gen-go. DO NOT EDIT.
// source: test_platform/steps/scheduler_traffic_split.proto

package steps

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	test_platform "go.chromium.org/chromiumos/infra/proto/go/test_platform"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

// SchedulerTrafficSplitRequest is the request to the step that determines the
// traffic split between backend schedulers.
type SchedulerTrafficSplitRequest struct {
	Request              *test_platform.Request `protobuf:"bytes,1,opt,name=request,proto3" json:"request,omitempty"`
	XXX_NoUnkeyedLiteral struct{}               `json:"-"`
	XXX_unrecognized     []byte                 `json:"-"`
	XXX_sizecache        int32                  `json:"-"`
}

func (m *SchedulerTrafficSplitRequest) Reset()         { *m = SchedulerTrafficSplitRequest{} }
func (m *SchedulerTrafficSplitRequest) String() string { return proto.CompactTextString(m) }
func (*SchedulerTrafficSplitRequest) ProtoMessage()    {}
func (*SchedulerTrafficSplitRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_263137ec0e6b85de, []int{0}
}

func (m *SchedulerTrafficSplitRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_SchedulerTrafficSplitRequest.Unmarshal(m, b)
}
func (m *SchedulerTrafficSplitRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_SchedulerTrafficSplitRequest.Marshal(b, m, deterministic)
}
func (m *SchedulerTrafficSplitRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_SchedulerTrafficSplitRequest.Merge(m, src)
}
func (m *SchedulerTrafficSplitRequest) XXX_Size() int {
	return xxx_messageInfo_SchedulerTrafficSplitRequest.Size(m)
}
func (m *SchedulerTrafficSplitRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_SchedulerTrafficSplitRequest.DiscardUnknown(m)
}

var xxx_messageInfo_SchedulerTrafficSplitRequest proto.InternalMessageInfo

func (m *SchedulerTrafficSplitRequest) GetRequest() *test_platform.Request {
	if m != nil {
		return m.Request
	}
	return nil
}

// SchedulerTrafficSplitResponse is the response from the step that determines
// the traffic split between backend schedulers.
type SchedulerTrafficSplitResponse struct {
	AutotestRequest      *test_platform.Request `protobuf:"bytes,1,opt,name=autotest_request,json=autotestRequest,proto3" json:"autotest_request,omitempty"`
	SkylabRequest        *test_platform.Request `protobuf:"bytes,2,opt,name=skylab_request,json=skylabRequest,proto3" json:"skylab_request,omitempty"`
	XXX_NoUnkeyedLiteral struct{}               `json:"-"`
	XXX_unrecognized     []byte                 `json:"-"`
	XXX_sizecache        int32                  `json:"-"`
}

func (m *SchedulerTrafficSplitResponse) Reset()         { *m = SchedulerTrafficSplitResponse{} }
func (m *SchedulerTrafficSplitResponse) String() string { return proto.CompactTextString(m) }
func (*SchedulerTrafficSplitResponse) ProtoMessage()    {}
func (*SchedulerTrafficSplitResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_263137ec0e6b85de, []int{1}
}

func (m *SchedulerTrafficSplitResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_SchedulerTrafficSplitResponse.Unmarshal(m, b)
}
func (m *SchedulerTrafficSplitResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_SchedulerTrafficSplitResponse.Marshal(b, m, deterministic)
}
func (m *SchedulerTrafficSplitResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_SchedulerTrafficSplitResponse.Merge(m, src)
}
func (m *SchedulerTrafficSplitResponse) XXX_Size() int {
	return xxx_messageInfo_SchedulerTrafficSplitResponse.Size(m)
}
func (m *SchedulerTrafficSplitResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_SchedulerTrafficSplitResponse.DiscardUnknown(m)
}

var xxx_messageInfo_SchedulerTrafficSplitResponse proto.InternalMessageInfo

func (m *SchedulerTrafficSplitResponse) GetAutotestRequest() *test_platform.Request {
	if m != nil {
		return m.AutotestRequest
	}
	return nil
}

func (m *SchedulerTrafficSplitResponse) GetSkylabRequest() *test_platform.Request {
	if m != nil {
		return m.SkylabRequest
	}
	return nil
}

func init() {
	proto.RegisterType((*SchedulerTrafficSplitRequest)(nil), "test_platform.steps.SchedulerTrafficSplitRequest")
	proto.RegisterType((*SchedulerTrafficSplitResponse)(nil), "test_platform.steps.SchedulerTrafficSplitResponse")
}

func init() {
	proto.RegisterFile("test_platform/steps/scheduler_traffic_split.proto", fileDescriptor_263137ec0e6b85de)
}

var fileDescriptor_263137ec0e6b85de = []byte{
	// 228 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0x32, 0x2c, 0x49, 0x2d, 0x2e,
	0x89, 0x2f, 0xc8, 0x49, 0x2c, 0x49, 0xcb, 0x2f, 0xca, 0xd5, 0x2f, 0x2e, 0x49, 0x2d, 0x28, 0xd6,
	0x2f, 0x4e, 0xce, 0x48, 0x4d, 0x29, 0xcd, 0x49, 0x2d, 0x8a, 0x2f, 0x29, 0x4a, 0x4c, 0x4b, 0xcb,
	0x4c, 0x8e, 0x2f, 0x2e, 0xc8, 0xc9, 0x2c, 0xd1, 0x2b, 0x28, 0xca, 0x2f, 0xc9, 0x17, 0x12, 0x46,
	0xd1, 0xa2, 0x07, 0xd6, 0x22, 0x25, 0x8d, 0x6a, 0x4e, 0x51, 0x6a, 0x61, 0x69, 0x6a, 0x31, 0x54,
	0x87, 0x52, 0x00, 0x97, 0x4c, 0x30, 0xcc, 0xc8, 0x10, 0x88, 0x89, 0xc1, 0x20, 0x03, 0x83, 0x20,
	0xaa, 0x84, 0x0c, 0xb8, 0xd8, 0xa1, 0x1a, 0x24, 0x18, 0x15, 0x18, 0x35, 0xb8, 0x8d, 0xc4, 0xf4,
	0x50, 0xed, 0x80, 0x2a, 0x0c, 0x82, 0x29, 0x53, 0x5a, 0xc8, 0xc8, 0x25, 0x8b, 0xc3, 0xc8, 0xe2,
	0x82, 0xfc, 0xbc, 0xe2, 0x54, 0x21, 0x47, 0x2e, 0x81, 0xc4, 0xd2, 0x92, 0x7c, 0xb0, 0x39, 0xc4,
	0x19, 0xce, 0x0f, 0x53, 0x0f, 0x73, 0x96, 0x2d, 0x17, 0x5f, 0x71, 0x76, 0x65, 0x4e, 0x62, 0x12,
	0xdc, 0x00, 0x26, 0xbc, 0x06, 0xf0, 0x42, 0x54, 0x43, 0xb9, 0x4e, 0xf6, 0x51, 0xb6, 0xe9, 0xf9,
	0x7a, 0xc9, 0x19, 0x45, 0xf9, 0xb9, 0x99, 0xa5, 0xb9, 0x7a, 0xf9, 0x45, 0xe9, 0xfa, 0x30, 0x4e,
	0x7e, 0xb1, 0x7e, 0x66, 0x5e, 0x5a, 0x51, 0xa2, 0x3e, 0x38, 0x7c, 0xf4, 0xd3, 0xf3, 0xf5, 0xb1,
	0x44, 0x43, 0x12, 0x1b, 0x58, 0xd6, 0x18, 0x10, 0x00, 0x00, 0xff, 0xff, 0x04, 0xd6, 0x35, 0x29,
	0xa4, 0x01, 0x00, 0x00,
}
