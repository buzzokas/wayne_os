/*
 * This file is part of the libsigrok project.
 *
 * Copyright 2014 Google, Inc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBSIGROK_HARDWARE_CHROMIUM_TWINKIE_PROTOCOL_H
#define LIBSIGROK_HARDWARE_CHROMIUM_TWINKIE_PROTOCOL_H

#include <stdint.h>
#include <glib.h>
#include "libsigrok.h"
#include "libsigrok-internal.h"

#define LOG_PREFIX "twinkie"
#define SAMPLE_UNIT_SIZE 1  /*in case we may want to change*/

/** Private, per-CC logical channel context. */
struct cc_context {
	int idx;
	int rollbacks;
	uint8_t prev_src;
	uint8_t level;
};

/** Private, per-device-instance driver context. */
struct dev_context {
	/** Maximum number of samples to capture, if nonzero. */
	uint64_t limit_samples;

	int64_t sent_samples;
	int submitted_transfers;
	uint8_t *convbuffer;
	size_t convbuffer_size;

	void *cb_data;
	unsigned int num_transfers;
	struct libusb_transfer **transfers;
	struct sr_context *ctx;

	struct cc_context cc[2];
};

struct sniffer_sample_header {
	uint16_t seq;
	uint16_t tstamp;
	uint16_t vbus_value; /* can be voltage or current */
	int16_t vbus_offset; /* the timestamp offset with respect to current packet */
};
#define SNIFFER_SAMPLE_PACKET_SIZE 64
#define SNIFFER_SAMPLE_HEADER_SIZE_WITHOUT_VBUS 4
#define SNIFFER_SAMPLE_HEADER_SIZE_WITH_VBUS (sizeof(struct sniffer_sample_header))
/*
 * use #define CONFIG_USBC_SNIFFER_HEADER_V1
 * if your twinkie does not send out vbus info;
 * use #define CONFIG_USBC_SNIFFER_HEADER_V2
 * if your twinkie sends out vbus info.
 */
#define CONFIG_USBC_SNIFFER_HEADER_V1


SR_PRIV int twinkie_start_acquisition(const struct sr_dev_inst *sdi);
SR_PRIV int twinkie_init_device(const struct sr_dev_inst *sdi);
SR_PRIV void twinkie_receive_transfer(struct libusb_transfer *transfer);

#endif
