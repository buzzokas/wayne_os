commit 60ff4dd9cd8a5052067b2dee1fc68bfba1f695cc
Author: Fangrui Song <maskray@google.com>
Date:   Thu Sep 12 08:55:17 2019 +0000

    [ELF] Support -z undefs
    
    -z undefs is the inverse of -z defs. It allows unresolved references
    from object files. This can be used to cancel --no-undefined or -z defs.
    
    Reviewed By: ruiu
    
    Differential Revision: https://reviews.llvm.org/D67479
    
    llvm-svn: 371715

diff --git a/lld/ELF/Driver.cpp b/lld/ELF/Driver.cpp
index 9cab10e49da..5022c81df78 100644
--- a/lld/ELF/Driver.cpp
+++ b/lld/ELF/Driver.cpp
@@ -386,7 +386,7 @@ static bool isKnownZFlag(StringRef s) {
          s == "noexecstack" || s == "nokeep-text-section-prefix" ||
          s == "norelro" || s == "noseparate-code" || s == "notext" ||
          s == "now" || s == "origin" || s == "relro" || s == "retpolineplt" ||
-         s == "rodynamic" || s == "text" || s == "wxneeded" ||
+         s == "rodynamic" || s == "text" || s == "undefs" || s == "wxneeded" ||
          s.startswith("common-page-size") || s.startswith("max-page-size=") ||
          s.startswith("stack-size=");
 }
@@ -517,6 +517,8 @@ static UnresolvedPolicy getUnresolvedSymbolPolicy(opt::InputArgList &args) {
     case OPT_z:
       if (StringRef(arg->getValue()) == "defs")
         return errorOrWarn;
+      if (StringRef(arg->getValue()) == "undefs")
+        return UnresolvedPolicy::Ignore;
       continue;
     }
   }
diff --git a/lld/test/ELF/zdefs.s b/lld/test/ELF/zdefs.s
index 21bec77b3bf..def217f6c0e 100644
--- a/lld/test/ELF/zdefs.s
+++ b/lld/test/ELF/zdefs.s
@@ -1,9 +1,15 @@
 # REQUIRES: x86
 # RUN: llvm-mc -filetype=obj -triple=x86_64-unknown-linux %s -o %t.o
+
+## Unresolved references from object files are allowed by default for -shared.
 # RUN: ld.lld -shared %t.o -o %t1.so
 
+## -z defs disallows unresolved references.
 # RUN: not ld.lld -z defs -shared %t.o -o %t1.so 2>&1 | FileCheck -check-prefix=ERR %s
 # ERR: error: undefined symbol: foo
 # ERR: >>> referenced by {{.*}}:(.text+0x1)
 
+## -z undefs allows unresolved references.
+# RUN: ld.lld -z defs -z undefs -shared %t.o -o /dev/null 2>&1 | count 0
+
 callq foo@PLT
