FSTACK = set(['-D__KERNEL__', '-fno-stack-protector', '-nodefaultlibs',
              '-nostdlib'])
FPIE = set(['-D__KERNEL__', '-fPIC', '-fPIE', '-fno-PIC', '-fno-PIE',
            '-fno-pic', '-fno-pie', '-fpic', '-fpie', '-nopie',
            '-nostartfiles', '-nostdlib', '-pie', '-static'])
PIE = set(['-D__KERNEL__', '-A', '-fno-PIC', '-fno-PIE', '-fno-pic', '-fno-pie',
           '-nopie', '-nostartfiles', '-nostdlib', '-pie', '-r', '--shared',
           '-shared', '-static'])
SSE = set(['-msse3', '-mssse3', '-msse4.1', '-msse4.2', '-msse4', '-msse4a'])

# Location of the root dir relative to this file
ROOT_REL_PATH = "../../../../.."

def update_flags(arch, myargs):
  # ARM32 specfic:
  # 1. Generate thumb codes by default.  GCC is configured with
  #    --with-mode=thumb and defaults to thumb mode already.  This
  #    changes the default behavior of clang and doesn't affect GCC.
  # 2. Do not force frame pointers on ARM32 (https://crbug.com/693137).
  if arch[-5:] == '-eabi':
    # No need to update flags in baremetal toolchains.
    return
  if arch[:5] in ('armv7', 'armv8'):
    FLAGS_TO_ADD.add('-mthumb')
    FLAGS_TO_ADD.discard('-fno-omit-frame-pointer')
  if FSTACK.intersection(myargs):
    FLAGS_TO_ADD.discard('-fstack-protector-strong')
    FLAGS_TO_ADD.add('-fno-stack-protector')
  if FPIE.intersection(myargs):
    FLAGS_TO_ADD.discard('-fPIE')
  if PIE.intersection(myargs):
    FLAGS_TO_ADD.discard('-pie')

def get_proc_cmdline(pid):
  with open('/proc/%i/cmdline' % pid) as fp:
    return fp.read().replace('\0', ' ')
  return None


def get_proc_status(pid, item):
  import re

  with open('/proc/%i/status' % pid) as fp:
    for line in fp:
      m = re.match(r'%s:\s*(.*)' % re.escape(item), line)
      if m:
        return m.group(1)
  return None


def log_parent_process_tree(log, ppid):
  depth = 0

  while ppid > 1:
    cmd = get_proc_cmdline(ppid)
    log.warning(' %*s {%5i}: %s' % (depth, '', ppid, cmd))

    ppid = get_proc_status(ppid, 'PPid')
    if not ppid:
      break
    ppid = int(ppid)
    depth += 2


def get_linker_path(cmd):
  """Return the a directory which contains an 'ld' that gcc is using."""

  # We did not pass the tuple i686-pc-linux-gnu to x86-32 clang. Instead,
  # we passed '-m32' to clang. As a result, clang does not want to use the
  # i686-pc-linux-gnu-ld, so we need to add this to help clang find the right
  # linker.
  for path in os.environ['PATH'].split(':'):
    cmd_path = os.path.join(path, cmd)
    if os.path.exists(cmd_path):
      if os.path.islink(cmd_path):
        cmd_path = os.readlink(cmd_path)
      return os.path.dirname(cmd_path)

  # When using the sdk outside chroot, we need to provide the cross linker path
  # to the compiler via -B ${linker_path}. This is because for gcc, it can
  # find the right linker via searching its internal paths. Clang does not have
  # such feature, and it falls back to $PATH search only. However, the path of
  # ${SDK_LOCATION}/bin is not necessarily in the ${PATH}. To fix this, we
  # provide the directory that contains the cross linker wrapper to clang.
  # Outside chroot, it is the top bin directory form the sdk tarball.
  sdk_bin_dir = os.path.join(os.path.dirname(os.path.realpath(sys.argv[0])),
                             ROOT_REL_PATH, 'bin')
  return os.path.abspath(sdk_bin_dir)


def get_gomacc_command():
  """Return the gomacc command if it is found in $GOMACC_PATH."""
  gomacc = os.environ.get('GOMACC_PATH')
  if gomacc and os.path.isfile(gomacc):
    return gomacc
  return None


def syntax_check_with_clang(clang_comp, clang_cmdline, print_cmdline):
  """Execute clang for syntax checking."""
  import subprocess

  command = [clang_comp] + clang_cmdline
  gomacc = get_gomacc_command()
  if gomacc:
    command.insert(0, gomacc)
  if print_cmdline:
    print('%s\n' % ' '.join(command))
  p = subprocess.Popen(command)
  p.wait()
  if p.returncode != 0:
    sys.exit(p.returncode)


def exec_and_bisect(execargs, bisect_stage, argv0, use_ccache):
  """Execute compiler, return and invoke bisection driver."""
  import bisect_driver

  bisect_dir = os.environ.get('BISECT_DIR', '/tmp/sysroot_bisect')
  try:
    ret = bisect_driver.bisect_driver(bisect_stage, bisect_dir, execargs)
  except OSError as e:
    handle_exec_exception(e, argv0, use_ccache, execargs)

  sys.exit(ret)


# Log 'time' like resource usage, along with corresponding command line, to
# GETRUSAGE file name if it is defined.
def log_rusage(log_file, argv0, execargs):
  import datetime
  import logging

  pid = os.fork()
  if pid > 0:
    t = datetime.datetime.now()
    _, status, usage = os.wait3(0)
    real = (datetime.datetime.now() - t).total_seconds()

    logger = logging.getLogger('sysroot_wrapper')
    logger.setLevel(logging.INFO)
    logger.addHandler(logging.FileHandler(log_file))
    logger.info('%f : %f : %f : %d : %s : %s',
                real, usage.ru_utime, usage.ru_stime, usage.ru_maxrss,
                argv0, ' '.join(execargs))

    # So that a root-created log is writable by others.
    try:
      os.chmod(log_file, 0o0666)
    except OSError:
      pass

    # Emulate child's exit status.
    if os.WIFEXITED(status):
      sys.exit(os.WEXITSTATUS(status))
    elif os.WIFSIGNALED(status):
      os.kill(os.getpid(), os.WTERMSIG(status))
    raise RuntimeError('wait: compilation process returned for unknown reason.')

def ccache_env_init(sysroot, use_clang):
  # Portage likes to set this for us when it has FEATURES=-ccache.
  # The other vars we need to setup manually because of tools like
  # scons that scrubs the env before we get executed.
  os.environ.pop('CCACHE_DISABLE', None)

  # We should be able to share the objects across compilers as
  # the pre-processed output will differ.  This allows boards
  # that share compiler flags (like x86 boards) to share caches.
  ccache_dir = '/var/cache/distfiles/ccache'
  os.environ['CCACHE_DIR'] = ccache_dir

  # If RESTRICT=sandbox is enabled, then sandbox won't be setup,
  # and the env vars won't be available for appending.
  if 'SANDBOX_WRITE' in os.environ:
    os.environ['SANDBOX_WRITE'] += ':%s' % ccache_dir

  # We need to get ccache to make relative paths from within the
  # sysroot.  This lets us share cached files across boards (if
  # all other things are equal of course like CFLAGS) as well as
  # across versions.  A quick test is something like:
  #   $ export CFLAGS='-O2 -g -pipe' CXXFLAGS='-O2 -g -pipe'
  #   $ BOARD=x86-alex
  #   $ cros_workon-$BOARD stop cros-disks
  #   $ emerge-$BOARD cros-disks
  #   $ cros_workon-$BOARD start cros-disks
  #   $ emerge-$BOARD cros-disks
  #   $ BOARD=amd64-generic
  #   $ cros_workon-$BOARD stop cros-disks
  #   $ emerge-$BOARD cros-disks
  #   $ cros_workon-$BOARD start cros-disks
  #   $ emerge-$BOARD cros-disks
  # All of those will get cache hits (ignoring the first one
  # which will seed the cache) due to this setting.
  if sysroot:
    os.environ['CCACHE_BASEDIR'] = sysroot

  # Minor speed up as we don't care about this in general.
  # os.environ['CCACHE_NOSTATS'] = 'no'
  # Useful for debugging.
  # os.environ['CCACHE_LOG'] = '/dev/stderr'

  # The GCC ebuild takes care of nuking the cache in the whenever it revbumps
  # in a way that matters, so we should be able to disable ccache's check.
  # We've found in practice though that sometimes that doesn't happen.  Since
  # the default check is cheap (it's a stat() in mtime mode), keep it enabled.
  # os.environ['CCACHE_COMPILERCHECK'] = 'none'

  # Make sure we keep the cached files group writable.
  os.environ['CCACHE_UMASK'] = '002'

  # ccache may generate false positive warnings.
  # Workaround bug https://crbug.com/649740
  if use_clang:
    os.environ['CCACHE_CPP2'] = 'yes'


def get_relocatable_root():
  """Get the root dir location relative to this wrapper's install dir."""
  install_dir = os.path.dirname(os.path.realpath(sys.argv[0]))
  root_dir = os.path.join(install_dir, ROOT_REL_PATH)
  return os.path.normpath(root_dir)

def main():
  gcc_flags = list(GCC_FLAGS_TO_ADD)
  clang_flags = list(CLANG_FLAGS_TO_ADD)
  use_clang_tidy = os.environ.get('WITH_TIDY')
  if use_clang_tidy:
     c_src_file = find_source_file(sys.argv)
     if not c_src_file:
       use_clang_tidy = False
  else:
     c_src_file = ''

  # Get target architecture, 'armv7a-cros-linux-gnueabi' or
  # 'x86_64-cros-linux-gnu', etc.
  gcc_comp = os.path.basename(sys.argv[0])
  arch = '-'.join(gcc_comp.split('-')[0:-1])

  myargs = sys.argv[1:]
  update_flags(arch, myargs)

  # Get clang binary location.
  clang_bin = os.path.join(get_relocatable_root(), 'usr/bin/clang')

  if not os.path.isabs(sys.argv[0]):
    # If sysroot_wrapper is invoked by relative path, call actual compiler in
    # relative form. This is neccesary to remove absolute path from compile
    # outputs.
    clang_bin = os.path.relpath(clang_bin)

  # If -clang-syntax is present or the command line uses clang instead
  # of GCC.
  invoke_clang = False

  # If -print-cmdline is present.
  print_cmdline = False

  # Filter unsupported linker flags if sanitizers are used.
  filter_unsupported_asan_flags = has_sanitizer_flags(myargs)

  # The following value is found/updated by
  # chromite/scripts/cros_setup_toolchain.py (in the function
  # _ProcessSysrootWrappers).
  use_ccache = True  # @CCACHE_DEFAULT@ Keep this comment for code.

  if '-noccache' in myargs or use_clang_tidy:
    use_ccache = False

  print_cmdline = '-print-cmdline' in myargs

  clang_cmdline = clang_flags + list(LLVM_NEXT_FLAGS_TO_ADD) + list(FLAGS_TO_ADD)

  gcc_cmdline = gcc_flags + list(FLAGS_TO_ADD)

  clang_codegen = sys.argv[0].split('-')[-1] in ('clang', 'clang++')

  if '-fstack-check' in myargs:
    print('Option "-fstack-check" is not supported. See crbug.com/485492',
          file=sys.stderr)
    sys.exit(1)

  invoke_clang = '-clang-syntax' in myargs or clang_codegen

  cmdline = [x for x in myargs if x not in WRAPPER_ONLY_OPTIONS]

  if filter_unsupported_asan_flags:
    cmdline = [x for x in cmdline if x not in UNSUPPORTED_ASAN_FLAGS]
    clang_cmdline = [x for x in clang_cmdline if x not in UNSUPPORTED_ASAN_FLAGS]

  # Add flags for fuzzer.
  if has_fuzzer_flags(myargs):
    clang_cmdline.extend(FUZZER_FLAGS_TO_ADD)

  if gcc_comp.startswith('x86_64') or startswith_i86(gcc_comp):
    cmdline.extend(X86_DISABLE_FLAGS)

  if not invoke_clang:
    # Create gcc command line filtering/converting the flags not supported
    # by gcc.
    for flag in cmdline:
      if flag in CLANG_TO_GCC.keys():
        gcc_cmdline.append(CLANG_TO_GCC[flag])
      elif flag not in GCC_UNSUPPORTED:
        gcc_cmdline.append(flag)
  else:

    # If these options are specified, do not run clang, even if -clang-syntax is
    # specified.
    # This is mainly for utilities that depend on compiler output.
    skip_clang_prefixes = ('-print-', '-dump', '@')
    skip_clang_set = set(['-', '-E', '-M'])

    # Reset gcc cmdline too. Only change is to remove -Xclang-only
    # options if specified.

    skip_clang = False
    for flag in cmdline:
      if (not clang_codegen and
          (flag.startswith(skip_clang_prefixes) or
           flag in skip_clang_set or
           flag.endswith('.S'))):
        skip_clang = True
      elif not (flag in CLANG_UNSUPPORTED or
                flag.startswith(CLANG_UNSUPPORTED_PREFIXES)):
        # Strip off -Xclang-only= if present.
        if flag.startswith('-Xclang-only='):
          opt = flag.partition('=')[2]
          clang_cmdline.append(opt)
          # No need to add to gcc_cmdline.
          continue
        elif flag.startswith('-Xclang-path='):
          import subprocess
          # Use clang installed at the given path.  But use the resource directory
          # for the main clang since the custom clang installation won't have
          # the cross-target libraries.
          resource_path = subprocess.check_output([clang_bin,
                                                   '--print-resource-dir'])
          clang_cmdline.append('-resource-dir=%s' % resource_path.strip())
          clang_cmdline.append('--gcc-toolchain=/usr')
          clang_path = flag.partition('=')[2]
          clang_bin = os.path.join(clang_path, 'clang')
          continue
        elif flag in GCC_TO_CLANG.keys():
          clang_cmdline.append(GCC_TO_CLANG[flag])
        elif flag not in CLANG_ARM_OPTIONS_TO_BE_DISCARDED:
          clang_cmdline.append(flag)
        else:
          tuple = arch.split('-')
          if not (tuple[0] == 'armv7a' and tuple[2] == 'linux'):
            clang_cmdline.append(flag)
      # Create gcc command line filtering unsupported flags.
      if flag in CLANG_TO_GCC.keys():
        gcc_cmdline.append(CLANG_TO_GCC[flag])
      elif flag not in GCC_UNSUPPORTED:
        gcc_cmdline.append(flag)

  sysroot = os.environ.pop('SYSROOT', '')
  if not sysroot:
    # Use the bundled sysroot by default.
    sysroot = os.path.join(get_relocatable_root(), 'usr', arch)

  # Pass sysroot if --sysroot flag is not set.
  sysroot_set = any(flag.startswith('--sysroot') for flag in cmdline)
  if not sysroot_set:
    clang_cmdline.insert(0, '--sysroot=%s' % sysroot)
    gcc_cmdline.insert(0, '--sysroot=%s' % sysroot)

  if invoke_clang and not skip_clang:
    clang_comp = os.environ.get('CLANG', clang_bin)
    clang_tidy_bin = clang_comp + '-tidy'

    # Check for clang or clang++.
    if sys.argv[0].endswith('++'):
      clang_comp += '++'

    # Specify the target for clang.
    linker = arch + '-ld'
    linker_path = os.path.relpath(get_linker_path(linker))
    clang_cmdline += ['-B' + linker_path]
    if startswith_i86(arch):
      # TODO: -target i686-pc-linux-gnu causes clang to search for
      # libclang_rt.asan-i686.a which doesn't exist because it's packaged
      # as libclang_rt.asan-i386.a. We can't use -target i386-pc-linux-gnu
      # because then it would try to run i386-pc-linux-gnu-ld which doesn't
      # exist. Consider renaming the runtime library to use i686 in its name.
      clang_cmdline += ['-m32']
      # clang does not support -mno-movbe. This is the alternate way to do it.
      clang_cmdline += ['-Xclang', '-target-feature', '-Xclang', '-movbe']
    else:
      clang_cmdline += ['-target', arch]

    if not clang_codegen:
      clang_cmdline.append('-fsyntax-only')
      # Enforce use of libstdc++ if clang is used only for syntax checks.
      # This avoids strange warnings when libc++ is default in clang.
      clang_cmdline.append('-stdlib=libstdc++')

    if not clang_codegen:
      syntax_check_with_clang(clang_comp, clang_cmdline, print_cmdline)

  execargs = []
  real_gcc = '%s.real' % sys.argv[0]
  gomacc = get_gomacc_command()
  if gomacc:
    argv0 = gomacc
    execargs += [gomacc]
  elif not use_ccache:
    argv0 = clang_comp if clang_codegen else real_gcc
  else:
    ccache_env_init(sysroot, clang_codegen)
    argv0 = '/usr/bin/ccache'
    execargs += ['ccache']

  if clang_codegen:
    execargs += [clang_comp] + clang_cmdline
  else:
    execargs += [real_gcc] + gcc_cmdline

  if not clang_codegen:
    use_clang_tidy = False

  if use_clang_tidy:
    import subprocess
    tidy_execargs = []
    default_tidy_checks = ','.join([
        '*',
        'google*',
        '-bugprone-narrowing-conversions',
        '-cppcoreguidelines-*',
        '-fuchsia-*',
        '-google-build-using-namespace',
        '-google-default-arguments',
        '-google-explicit-constructor',
        '-google-readability*',
        '-google-runtime-int',
        '-google-runtime-references',
        '-hicpp-avoid-c-arrays',
        '-hicpp-braces-around-statements',
        '-hicpp-no-array-decay',
        '-hicpp-signed-bitwise',
        '-hicpp-uppercase-literal-suffix',
        '-hicpp-use-auto',
        '-llvm-namespace-comment',
        '-misc-non-private-member-variables-in-classes',
        '-misc-unused-parameters',
        '-modernize-*',
        '-readability-*',
    ])
    tidy_args = ['-checks=' + default_tidy_checks]
    resource_path = subprocess.check_output([clang_comp,
                                             '--print-resource-dir'])
    resource_dir_arg = ['-resource-dir=%s' % resource_path.strip()]
    tidy_execargs += [clang_tidy_bin] + tidy_args + [c_src_file] + ['--'] \
                     + resource_dir_arg + clang_cmdline
    if print_cmdline:
      print("Calling clang-tidy:")
      print(repr(tidy_execargs))

    retval = subprocess.call(tidy_execargs)
    if retval != 0 and print_cmdline:
      print("Call to clang-tidy failed!")

  if print_cmdline:
    print('[%s] %s' % (argv0, ' '.join(execargs)))

  sys.stdout.flush()

  getrusage = os.environ.get('GETRUSAGE')
  bisect_stage = os.environ.get('BISECT_STAGE')

  if getrusage and bisect_stage:
    raise RuntimeError('GETRUSAGE is meaningless with BISECT_STAGE')

  if getrusage:
    log_rusage(getrusage, argv0, execargs)

  # We'll sometimes pull in a new compiler (or similar) and want a good way of
  # vetting it. New warnings are useful, but actively block the potentially more
  # interesting things (actually building the world, testing, etc). This is our
  # escape hatch.
  if os.getenv('FORCE_DISABLE_WERROR'):
    sys.exit(double_build_with_wno_error(execargs))

  if not bisect_stage:
    try:
      os.execv(argv0, execargs)
    except OSError as e:
      handle_exec_exception(e, argv0, use_ccache, execargs)

  # Only comes here if doing bisection.
  return exec_and_bisect(execargs, bisect_stage, argv0, use_ccache)


if __name__ == '__main__':
  sys.exit(main())
