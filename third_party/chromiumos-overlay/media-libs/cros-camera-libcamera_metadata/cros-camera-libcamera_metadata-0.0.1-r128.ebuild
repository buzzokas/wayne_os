# Copyright 2017 The Chromium OS Authors. All rights reserved.
# Distributed under the terms of the GNU General Public License v2

EAPI=5

CROS_WORKON_COMMIT="4c3891efd6a13d2c7b3a3e35d27fa92bf00e2b4d"
CROS_WORKON_TREE=("dc1506ef7c8cfd2c5ffd1809dac05596ec18773c" "c1d6f47d810546f7412d95791a85fb4d35831af3" "de8fa4cecc59ae774f2a38fb5c4697e410ab9a22" "a9c9dfedee8947f546a02e996ac05ea263acfaa1")
CROS_WORKON_PROJECT="chromiumos/platform2"
CROS_WORKON_LOCALNAME="../platform2"
CROS_WORKON_SUBTREE=".gn camera/build camera/android common-mk"
CROS_WORKON_OUTOFTREE_BUILD="1"
CROS_WORKON_INCREMENTAL_BUILD="1"

PLATFORM_SUBDIR="camera"
PLATFORM_GYP_FILE="android/libcamera_metadata/libcamera_metadata.gyp"

inherit cros-camera cros-workon platform

DESCRIPTION="Android libcamera_metadata"

LICENSE="BSD-Google"
SLOT="0"
KEYWORDS="*"
IUSE="-asan"

RDEPEND="!media-libs/arc-camera3-libcamera_metadata"

DEPEND="${RDEPEND}
	media-libs/cros-camera-android-headers"

src_install() {
	local INCLUDE_DIR="/usr/include/android"
	local LIB_DIR="/usr/$(get_libdir)"
	local SRC_DIR="android/libcamera_metadata"
	local PC_FILE_TEMPLATE="${SRC_DIR}/libcamera_metadata.pc.template"
	local PC_FILE="${WORKDIR}/${PC_FILE_TEMPLATE##*/}"
	PC_FILE="${PC_FILE%%.template}"

	dolib.so "${OUT}/lib/libcamera_metadata.so"

	insinto "${INCLUDE_DIR}/system"
	doins "${SRC_DIR}/include/system"/*.h
	# Install into the system folder to avoid cros lint complaint of "include the
	# directory when naming .h files"
	doins "${SRC_DIR}/include/camera_metadata_hidden.h"

	sed -e "s|@INCLUDE_DIR@|${INCLUDE_DIR}|" -e "s|@LIB_DIR@|${LIB_DIR}|" \
		"${PC_FILE_TEMPLATE}" > "${PC_FILE}"
	insinto "${LIB_DIR}/pkgconfig"
	doins "${PC_FILE}"
}
