# Copyright 2018 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

TIME="SHORT"
AUTHOR = "The Chromium OS Authors"
DOC = """
Verified boot ensures only verified code runs after reboot. Malicious data from
the stateful partition can still influence verified code to perform unintended
actions that may lead to device compromise. One vector that has been proven
particularly successful is to plant malicious symlinks on the stateful file
system, causing privileged code that writes through an affected path to be
tricked into writing to an unintended file system location controlled by the
attacker. Another observed vector has been the conversion of normal files into
FIFOs as to block processes that use synchronous I/O to read the file, not
expecting it to be a FIFO. This test verifies that the kernel has been set up
to deny symlink traversal and opening of FIFOs on the stateful file system.
"""
NAME = "security_StatefulPartitionHardening"
PURPOSE = """
To prevent verified software accessing the stateful file system from
inadvertently writing through malicious symlinks or opening files that have
been converted into FIFOs.
"""
CRITERIA = """
The test verifies that opening a FIFO is totally prohibited on the stateful
file system, and symlink traversal is blocked except for a few white-listed
directories.
"""
ATTRIBUTES = "suite:bvt-inline"
TEST_CLASS = "security"
TEST_CATEGORY = "Functional"
TEST_TYPE = "client"
JOB_RETRIES = 2

job.run_test("security_StatefulPartitionHardening")
