# -*- coding: utf-8 -*-
# # Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
# # Use of this source code is governed by a BSD-style license that can be
# # found in the LICENSE file.

import os
import sys

PKGDIR = __path__[0]
# This should be the third_party/whining directory.
BASE_DIR = os.path.realpath(os.path.join(PKGDIR, '..', '..'))

sys.path.append(BASE_DIR)
sys.path.append(os.path.join(BASE_DIR, 'src'))
sys.path.append(os.path.join(BASE_DIR, 'src/backend'))


CHROMIUMOS_DIR = os.path.realpath(os.path.join(BASE_DIR, '..', '..', '..'))

# More specialized logic can be used here.
_CHROMITE_VENV_DIR = os.path.realpath(os.path.join(
    CHROMIUMOS_DIR, 'chromite', 'venv'))
sys.path.append(CHROMIUMOS_DIR)
sys.path.append(_CHROMITE_VENV_DIR)

try:
    import chromite
except ImportError as e:
    raise Exception('Error when importing chromite (venv dir: %s): %s'
                    % (_CHROMITE_VENV_DIR, e))
