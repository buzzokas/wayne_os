# Copyright 2016 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

# Simple logging func used as:
# CALL log_msg('Finished doing xyz');
delimiter $$
DROP PROCEDURE IF EXISTS log_msg$$
CREATE PROCEDURE log_msg(message VARCHAR(512))
BEGIN
    INSERT INTO msg_log (msg)
    VALUES (message);
END$$
delimiter ;


# A convenience function that shows time that passed between consecutive entries
# Example output:
#
# mysql> call show_log();
# +------------------------------------+----------+---------------------+
# | msg                                | delta    | msg_time            |
# +------------------------------------+----------+---------------------+
# | Finished updating dict tables      | 00:00:34 | 2013-04-17 00:51:33 |
# | Start removing old data            | 00:00:00 | 2013-04-17 00:51:33 |
# | Finished removing old data         | 00:00:00 | 2013-04-17 00:51:33 |
# | Started copying tests from view    | 00:00:01 | 2013-04-17 00:51:34 |
# | Finished copying tests from view   | 00:04:40 | 2013-04-17 00:56:14 |
# | Started copying bad tests          | 00:00:00 | 2013-04-17 00:56:14 |
# | Finished copying bad tests         | 00:12:12 | 2013-04-17 01:08:26 |
# | Started copying good tests         | 00:00:00 | 2013-04-17 01:08:26 |
# | Finished copying good tests        | 00:01:43 | 2013-04-17 01:10:09 |
#   ...
#
delimiter $$
DROP PROCEDURE IF EXISTS show_log$$
CREATE PROCEDURE show_log()
BEGIN
    select a.msg, timediff(a.msg_time, b.msg_time) as delta, a.msg_time
    from msg_log a
    join msg_log b
    on (a.msg_idx = b.msg_idx + 1);
END$$


# This function is called at the end of update_wmdb.py script. It updates the
# dictionary tables, copies new data to main tables and computes batch stats.
delimiter $$
DROP PROCEDURE IF EXISTS copy_imported$$
CREATE PROCEDURE copy_imported(is_init TINYINT)
BEGIN
    DECLARE max_data_time DATETIME;
    DECLARE good_status_idx TINYINT;
    # Disable the periodic event that looks for missing things. For init, enable
    # manually after starting incremental updates.
    ALTER EVENT find_missing DISABLE;

    ################# Update dictionary tables #################
    START TRANSACTION;
        DELETE FROM tko_machines;
        INSERT INTO tko_machines
        SELECT * FROM import_tko_machines order by machine_idx;

        DELETE FROM tko_status;
        INSERT INTO tko_status
        SELECT * FROM import_tko_status;
    COMMIT;

    SET good_status_idx = (
      SELECT status_idx FROM tko_status WHERE word = 'GOOD');

    CALL log_msg('Start updating dict tables');
    START TRANSACTION;
        INSERT IGNORE INTO suites (suite)
        SELECT distinct suite
        FROM import_jobs;
    COMMIT;

    START TRANSACTION;
        INSERT IGNORE INTO test_names (test_name)
        SELECT distinct test
        FROM import_tko_tests
        JOIN import_jobs USING (job_idx);
        # The JOIN is because we don't want test from jobs we left out earlier.
    COMMIT;

    START TRANSACTION;
        INSERT IGNORE INTO configs (config)
        SELECT distinct config
        FROM import_images
        ORDER BY config;
    COMMIT;

    START TRANSACTION;
        INSERT IGNORE INTO configs (config)
        SELECT distinct build_config
        FROM import_jobs;
    COMMIT;

    START TRANSACTION;
        INSERT IGNORE INTO builds (build, first_seen_time)
        SELECT buildname, min(start_time)
        FROM import_images
        GROUP BY buildname
        ORDER BY min(start_time);
    COMMIT;

    START TRANSACTION;
        INSERT IGNORE INTO builds (build, first_seen_time)
        SELECT buildname, min(queued_time)
        FROM import_jobs
        GROUP BY buildname
        ORDER BY min(queued_time);
    COMMIT;

    START TRANSACTION;
        INSERT IGNORE INTO platforms (platform)
        SELECT distinct machine_group
        FROM tko_machines
        JOIN (
          SELECT distinct machine_idx FROM import_tko_tests) AS m
          USING (machine_idx)
        ORDER BY machine_group;
    COMMIT;
    CALL log_msg('Finished updating dict tables');

    ##### Service tables #####
    START TRANSACTION;
        # TODO: should we take care of suites in schedule but not yet in data
        UPDATE import_suite_schedule s
        JOIN suites USING (suite)
        SET s.suite_idx = suites.suite_idx;

        REPLACE INTO suite_schedule
        SELECT * FROM import_suite_schedule;

        REPLACE INTO autobugs
        SELECT * FROM import_autobugs;
    COMMIT;

    # Remove data that is too old to prevent DB from growing indefinitely
    CALL log_msg('Start removing old data');
    CALL remove_old_data();
    CALL log_msg('Finished removing old data');

    # Invalidate tests that were previously imported.
    # Some tests might first be imported and later marked as
    # invalid in the autotest db. We need to set their invalid bit
    # in good_tests and bad_tests.
    CALL log_msg('Start marking invalid tests in good_tests and bad_tests');
    CALL invalidate_existing_tests();
    CALL log_msg('Finished marking invalid tests in good_tests and bad_tests');

    ############### Main tables #################################
    CREATE TEMPORARY TABLE new_tests like good_tests;
    ALTER TABLE new_tests
    DROP INDEX key_tbp,
    DROP INDEX key_rsbpt_good;

    CALL log_msg('Started copying tests from view');
    START TRANSACTION;
        INSERT INTO new_tests
        SELECT * FROM imported_tests_view
        ORDER BY test_idx;
    COMMIT;
    CALL log_msg('Finished copying tests from view');


    CALL log_msg('Started copying bad tests');
    START TRANSACTION;
        REPLACE INTO bad_tests
        SELECT * FROM new_tests
        WHERE status_idx <> good_status_idx
        ORDER BY test_idx;
    COMMIT;
    CALL log_msg('Finished copying bad tests');


    CALL log_msg('Started copying good tests');
    START TRANSACTION;
        REPLACE INTO good_tests
        SELECT * FROM new_tests
        WHERE status_idx = good_status_idx
        ORDER BY test_idx;
    COMMIT;
    CALL log_msg('Finished copying good tests');


    #### platform_configs
    CALL log_msg('Start updating platform_configs');
    START TRANSACTION;
        # Update the platform field in builds, this should be after update to
        # tests to have all the latest test runs. Otherwise we might miss new
        # platforms.
        INSERT IGNORE INTO platform_configs
        SELECT DISTINCT config_idx, platform_idx
        FROM new_tests;
    COMMIT;
    CALL log_msg('Finished updating platform_configs');

    # TODO: is there anything we can do to make this look nicer?
    START TRANSACTION;
        REPLACE INTO images (
            release_number, build_idx, platform_idx, config_idx, buildbot_root,
            builder_name, board, `number`, completed, result, simplified_result,
            start_time, end_time, chromever, reason)
        SELECT
            release_number,
            build_idx,
            platform_idx,
            config_idx,
            buildbot_root,
            builder_name,
            board,
            `number`,
            completed,
            result,
            simplified_result,
            start_time,
            end_time,
            chromever,
            reason
        FROM import_images
        JOIN builds ON (buildname = builds.build)
        JOIN configs USING (config)
        JOIN platform_configs USING (config_idx);
    COMMIT;

    # Calculate some aggregates
    CALL calc_import_stats(is_init);

    IF not is_init THEN
        # Delete newly appearing tests from the list of missing tests, but only if
        # there are no comments on them.
        CALL log_msg('Started removing newly appearing tests/suites from missing');
        DELETE missing_tests
        FROM missing_tests
        JOIN new_tests
            USING (release_number, platform_idx, build_idx, suite_idx, test_name_idx)
        LEFT JOIN mt_comments AS c
            USING (missing_test_idx)
        WHERE c.missing_test_idx IS NULL;

        # Same as above for missing suites.
        DELETE missing_suites
        FROM missing_suites
        JOIN upd_suite_runs
            USING (release_number, platform_idx, build_idx, suite_idx)
        LEFT JOIN ms_comments AS c
            USING (missing_suite_idx)
        WHERE c.missing_suite_idx IS NULL;
        CALL log_msg('Finished removing newly appearing tests/suites from missing');
    END IF;

    # Call the procs to find missing things. Use latest timestamp seen in the
    # imported data to avoid false alarms about missing tests. Looking only at
    # good_tests as a 'good enough' approximation.
    SET max_data_time = (SELECT max(test_started_time) FROM good_tests);
    CALL find_missing_suites(max_data_time);
    CALL find_missing_tests(max_data_time);

    ALTER EVENT find_missing ENABLE;

END$$
delimiter ;



delimiter $$
DROP PROCEDURE IF EXISTS calc_import_stats$$
CREATE PROCEDURE calc_import_stats(is_init TINYINT)
# is_init tells whether this is incremental update or initialization of empty DB
BEGIN
    START TRANSACTION;
        # TODO (if init, can be done from tests table)
        INSERT IGNORE INTO known_tests
        SELECT distinct release_number, platform_idx, suite_idx, test_name_idx
        FROM new_tests;
    COMMIT;

    CALL log_msg('Start upd_suite_runs');
    START TRANSACTION;
        TRUNCATE TABLE upd_suite_runs;
        INSERT INTO upd_suite_runs
        SELECT release_number, platform_idx, suite_idx, build_idx,
            max(test_started_time), max(test_finished_time)
        FROM new_tests
        GROUP BY release_number, platform_idx, suite_idx, build_idx;
    COMMIT;
    CALL log_msg('Finished upd_suite_runs');

    # get the status_idx for status 'RUNNING'
    SET @running_status = (
        SELECT status_idx FROM tko_status WHERE word = 'RUNNING'
        );

    CALL log_msg('Start stats_rpsb');
    START TRANSACTION;
        # stats_rpsb
        # For images seen in the new data, take all known test runs and
        # (re)compute per-build stats.
        # For incremental update, the calculation is done for new suite runs
        # only, but for initialization of empty db with large data set, the
        # query without join runs much faster (by a factor of ~50).
        IF is_init THEN
            REPLACE INTO stats_rpsb
            SELECT release_number, platform_idx, suite_idx, build_idx,
                max(test_finished_time) AS last_finished_time,
                min(test_started_time)  AS first_started_time,
                TIMESTAMPDIFF(SECOND, min(test_started_time),
                max(test_finished_time)) AS seconds_duration,
                max(status_idx = @running_status) AS was_running
            FROM new_tests
            GROUP BY release_number, platform_idx, suite_idx, build_idx;
        ELSE
            REPLACE INTO stats_rpsb
            SELECT release_number, platform_idx, suite_idx, build_idx,
                max(test_finished_time) AS last_finished_time,
                min(test_started_time)  AS first_started_time,
                TIMESTAMPDIFF(SECOND, min(test_started_time),
                max(test_finished_time)) AS seconds_duration,
                max(status_idx = @running_status) AS was_running
            FROM (
                SELECT *
                FROM upd_suite_runs
                JOIN good_tests
                USING (release_number, platform_idx, suite_idx, build_idx)

                UNION

                SELECT *
                FROM upd_suite_runs
                JOIN bad_tests
                USING (release_number, platform_idx, suite_idx, build_idx)
            ) AS r
            GROUP BY release_number, platform_idx, suite_idx, build_idx;
        END IF;
    COMMIT;
    CALL log_msg('Finished stats_rpsb');

    CALL log_msg('Start suite_stats');
    START TRANSACTION;
        ## recalc suite stats from scratch
        # TODO: remove some fields in this table, many are unused
        TRUNCATE TABLE suite_stats;

        REPLACE INTO suite_stats
        SELECT stats.*, suite_schedule.run_on from (
            SELECT release_number, platform_idx, suite_idx,
                avg(seconds_duration)   AS avg_duration,
                std(seconds_duration)   AS std_duration,
                max(build_idx)     AS latest_build_idx,
                min(first_started_time) AS first_seen_time,
                max(last_finished_time) AS last_seen_time,
                count(1)                AS runs_total,
                datediff(max(last_finished_time),
                min(first_started_time)) AS days_observed,
                count(1) * 7 / datediff(max(last_finished_time), min(first_started_time)) AS runs_per_week
            FROM stats_rpsb
            WHERE not was_running
            GROUP BY release_number, platform_idx, suite_idx
            ) stats
        JOIN suite_schedule USING (suite_idx);
    COMMIT;
    CALL log_msg('Finished suite_stats');

    # Push new objects into the queue tables to check for missing tests,
    # suites and builds when the time comes.
    CALL log_msg('Start check_missing');
    START TRANSACTION;
        REPLACE INTO check_missing_tests
            (release_number, platform_idx, suite_idx, build_idx, queued_time,
                expected_time, check_after_time)
        SELECT release_number, platform_idx, suite_idx, build_idx,
            utc_timestamp() AS queued_time,
            max_started_time AS expected_time,
            adddate(max_started_time, interval test_alert_hours hour)
        FROM upd_suite_runs
        JOIN suite_schedule USING (suite_idx)
        JOIN alert_age USING (run_on)
        where suite <> 'browsertests';
        # TODO (kamrik): figure what to do with browsertests, they add too much noise
    COMMIT;

    START TRANSACTION;
        REPLACE INTO check_missing_suites
            (release_number, platform_idx, build_idx, run_on, queued_time,
                expected_time, check_after_time)
        SELECT release_number, platform_idx, build_idx, run_on,
            utc_timestamp() AS queued_time,
            max_started_time AS expected_time,
            adddate(max_started_time, interval suite_alert_hours hour)
        FROM
            (
            SELECT release_number, platform_idx, build_idx, run_on,
                max(max_started_time) AS max_started_time
            FROM upd_suite_runs
            JOIN suite_schedule USING (suite_idx)
            GROUP BY release_number, platform_idx, build_idx, run_on
            ) AS rpbf
        JOIN alert_age USING (run_on);
    COMMIT;

    /* ## missing builds are unused
    START TRANSACTION;
        REPLACE INTO check_missing_builds
            (release_number, build, run_on, queued_time,
                expected_time, check_after_time)
        SELECT release_number, build_idx, run_on,
            utc_timestamp() AS queued_time,
            max_started_time AS expected_time,
            adddate(max_started_time, interval build_alert_hours hour)
        FROM
            (
            SELECT release_number, build_idx, run_on,
                max(max_started_time) AS max_started_time
            FROM upd_suite_runs
            JOIN suite_schedule USING (suite_idx)
            GROUP BY release_number, build_idx, run_on
            ) rpbf
        JOIN alert_age USING (run_on);
    COMMIT;*/
    CALL log_msg('Finished check_missing');
END$$
delimiter ;


delimiter $$
DROP PROCEDURE IF EXISTS remove_old_data$$
CREATE PROCEDURE remove_old_data()
BEGIN
    # TODO (kamrik): make sure the appropriate date/time columns are indexed.
    # There are several options in each table.
    DECLARE old_date DATE;
    SET old_date = adddate(utc_date(), -100);
    START TRANSACTION;
        DELETE FROM good_tests WHERE test_started_time < old_date;
        DELETE FROM bad_tests WHERE test_started_time < old_date;
        DELETE FROM stats_rpsb WHERE last_finished_time < old_date;
    COMMIT;
END$$
delimiter ;


delimiter $$
DROP PROCEDURE IF EXISTS invalidate_existing_tests$$
CREATE PROCEDURE invalidate_existing_tests()
BEGIN
    START TRANSACTION;
        UPDATE good_tests t
        JOIN import_tko_tests i
        ON t.test_idx = i.invalidates_test_idx
        SET t.invalid = 1,
            t.retry_orig_test_idx = IF (t.invalidates_test_idx IS NULL,
            t.test_idx, t.retry_orig_test_idx)
        WHERE i.invalidates_test_idx IS NOT NULL;

        UPDATE bad_tests t
        JOIN import_tko_tests i
        ON t.test_idx = i.invalidates_test_idx
        SET t.invalid = 1,
            t.retry_orig_test_idx = IF (t.invalidates_test_idx IS NULL,
            t.test_idx, t.retry_orig_test_idx)
        WHERE i.invalidates_test_idx IS NOT NULL;
    COMMIT;
END$$
delimiter ;


delimiter $$
DROP PROCEDURE IF EXISTS find_missing_tests$$
CREATE PROCEDURE find_missing_tests(cut_time DATETIME)
BEGIN
    DECLARE time_now datetime;
    CALL log_msg('Started find_missing_tests');
    START TRANSACTION;
        SET time_now = UTC_TIMESTAMP();

        REPLACE INTO missing_tests
            (release_number, platform_idx, suite_idx, build_idx, test_name_idx,
             expected_time, detected_time)
        SELECT release_number, platform_idx, suite_idx, build_idx, test_name_idx, expected_time,
            time_now
        FROM check_missing_tests
        JOIN known_tests
          USING (release_number, platform_idx, suite_idx)
        # the above is a list of all expected test runs, same format as the list
        # of actual runs below
        LEFT JOIN good_tests
          USE INDEX FOR JOIN (key_rsbpt_good)
          USING (release_number, platform_idx, suite_idx, build_idx, test_name_idx)
        LEFT JOIN bad_tests
          USE INDEX FOR JOIN (key_rsbpt_bad)
          USING (release_number, platform_idx, suite_idx, build_idx, test_name_idx)
        WHERE good_tests.test_name_idx is NULL
        AND bad_tests.test_name_idx is NULL
        AND check_after_time <= cut_time;


        DELETE FROM check_missing_tests
        WHERE check_after_time <= cut_time;
    COMMIT;
    CALL log_msg('Finished find_missing_tests');
END$$
delimiter ;


delimiter $$
DROP PROCEDURE IF EXISTS find_missing_suites$$
CREATE PROCEDURE find_missing_suites(cut_time DATETIME)
BEGIN
    DECLARE time_now datetime;
    CALL log_msg('Started find_missing_suites');
    START TRANSACTION;
        SET time_now = UTC_TIMESTAMP();

        REPLACE INTO missing_suites
            (release_number, platform_idx, build_idx, suite_idx, expected_time, detected_time)
        SELECT release_number, platform_idx, build_idx, suite_idx, expected_time, time_now
        FROM (
            SELECT *
            FROM check_missing_suites
            WHERE check_after_time <= cut_time
            ) AS rpbf1
        JOIN suite_stats
        USING (release_number, platform_idx, run_on)
        # above are the expected suite runs, below are those really observed
        LEFT JOIN stats_rpsb
        USING (release_number, platform_idx, suite_idx, build_idx)
        WHERE stats_rpsb.suite_idx is NULL;


        DELETE FROM check_missing_suites where check_after_time <= cut_time;
    COMMIT;
    CALL log_msg('Finished find_missing_suites');
END$$
delimiter ;

delimiter $$
DROP PROCEDURE IF EXISTS add_triage_comment$$
CREATE PROCEDURE add_triage_comment (comment_test_idx int, comment_text varchar(2048))
BEGIN
    DECLARE prev_idx int unsigned default NULL;
    DECLARE new_idx int unsigned default NULL;

    START TRANSACTION;

        SET prev_idx = (
            SELECT comment_idx
            FROM triage_comments
            WHERE test_idx = comment_test_idx
                AND replaced_by is NULL
        );

        INSERT INTO triage_comments (
            test_idx, comment, test_name_idx, suite_idx, build_idx,
            release_number, platform_idx, status_idx, reason, created_time)
        SELECT comment_test_idx, comment_text, test_name_idx, suite_idx,
            build_idx, release_number, platform_idx, status_idx, reason,
            utc_timestamp() AS created_time
        FROM bad_tests
        WHERE test_idx = comment_test_idx;

        SET new_idx = LAST_INSERT_ID();

        IF prev_idx IS NOT NULL THEN
            UPDATE triage_comments
            SET replaced_by=new_idx
            WHERE comment_idx=prev_idx;
        END IF;
    COMMIT;
END$$
delimiter ;


delimiter $$
DROP PROCEDURE IF EXISTS add_mt_comment$$
CREATE PROCEDURE add_mt_comment (mt_idx int, comment_text varchar(2048))
# Add a comment for missing test.
BEGIN
    DECLARE prev_idx int unsigned default NULL;
    DECLARE new_idx int unsigned default NULL;

    START TRANSACTION;

        SET prev_idx = (
            SELECT mt_comment_idx
            FROM mt_comments
            WHERE missing_test_idx = mt_idx
                AND replaced_by is NULL
        );

        INSERT INTO mt_comments (
            missing_test_idx, comment, test_name_idx, suite_idx, build_idx,
            release_number, platform, created_time)
        SELECT mt_idx, comment_text,
            test_name_idx, suite_idx, build_idx, release_number, platform_idx,
            utc_timestamp() AS created_time
        FROM missing_tests
        WHERE missing_test_idx = mt_idx;

        SET new_idx = LAST_INSERT_ID();

        IF prev_idx IS NOT NULL THEN
            UPDATE mt_comments
            SET replaced_by=new_idx
            WHERE mt_comment_idx=prev_idx;
        END IF;
    COMMIT;
END$$
delimiter ;


delimiter $$
DROP PROCEDURE IF EXISTS add_ms_comment$$
CREATE PROCEDURE add_ms_comment (ms_idx int, comment_text varchar(2048))
# Add a comment for missing suite.
BEGIN
    DECLARE prev_idx int unsigned default NULL;
    DECLARE new_idx int unsigned default NULL;

    START TRANSACTION;

        SET prev_idx = (
            SELECT ms_comment_idx
            FROM ms_comments
            WHERE missing_suite_idx = ms_idx
                AND replaced_by is NULL
        );

        INSERT INTO ms_comments (
            missing_suite_idx, comment, suite_idx, build_idx, release_number,
            platform, created_time)
        SELECT ms_idx, comment_text,
            suite_idx, build_idx, release_number, platform_idx,
            utc_timestamp() AS created_time
        FROM missing_suites
        WHERE missing_suite_idx = ms_idx;

        SET new_idx = LAST_INSERT_ID();

        IF prev_idx IS NOT NULL THEN
            UPDATE ms_comments
            SET replaced_by=new_idx
            WHERE ms_comment_idx=prev_idx;
        END IF;
    COMMIT;
END$$
delimiter ;

delimiter $$
DROP PROCEDURE IF EXISTS clean_import_tables$$
CREATE PROCEDURE clean_import_tables()
BEGIN
    TRUNCATE TABLE import_tko_machines;
    TRUNCATE TABLE import_tko_status;
    TRUNCATE TABLE import_jobs;
    TRUNCATE TABLE import_tko_tests;
    TRUNCATE TABLE import_images;
    TRUNCATE TABLE import_suite_schedule;
    TRUNCATE TABLE import_autobugs;
END$$
delimiter ;

delimiter $$
DROP PROCEDURE IF EXISTS update_retry_info$$
CREATE PROCEDURE update_retry_info()
BEGIN
    CALL log_msg('Start updating retry info');
    # Note the correctness of this function relies on
    # the assumption that tests are imported in the order of time.
    # Using test_idx would cut in the middle of a retry chain.
    # If not importing tests in the order of time, use build_idx instead
    # of test_idx, which would be slower.
    SET @starting_idx = (SELECT min(test_idx) FROM imported_tests_view);

    START TRANSACTION;
        UPDATE good_tests
        SET retry_orig_test_idx = test_idx
        WHERE invalid = 1 AND invalidates_test_idx IS NULL
              AND test_idx >= @starting_idx;

        UPDATE bad_tests
        SET retry_orig_test_idx = test_idx
        WHERE invalid = 1 AND invalidates_test_idx IS NULL
              AND test_idx >= @starting_idx;

        REPEAT
            SET @count = 0;

            UPDATE good_tests t1
            JOIN good_tests t2 ON (t2.test_idx = t1.invalidates_test_idx)
            SET t1.retry_orig_test_idx = t2.retry_orig_test_idx
            WHERE t1.retry_orig_test_idx IS NULL
                  AND t2.retry_orig_test_idx IS NOT NULL
                  AND t1.test_idx >= @starting_idx;
            SET @count = (SELECT ROW_COUNT()) + @count;

            UPDATE bad_tests t1
            JOIN good_tests t2 ON (t2.test_idx = t1.invalidates_test_idx)
            SET t1.retry_orig_test_idx = t2.retry_orig_test_idx
            WHERE t1.retry_orig_test_idx IS NULL
                  AND t2.retry_orig_test_idx IS NOT NULL
                  AND t1.test_idx >= @starting_idx;
            SET @count = (SELECT ROW_COUNT()) + @count;

            UPDATE good_tests t1
            JOIN bad_tests t2 ON (t2.test_idx = t1.invalidates_test_idx)
            SET t1.retry_orig_test_idx = t2.retry_orig_test_idx
            WHERE t1.retry_orig_test_idx IS NULL
                  AND t2.retry_orig_test_idx IS NOT NULL
                  AND t1.test_idx >= @starting_idx;
            SET @count = (SELECT ROW_COUNT()) + @count;

            UPDATE bad_tests t1
            JOIN bad_tests t2 ON (t2.test_idx = t1.invalidates_test_idx)
            SET t1.retry_orig_test_idx = t2.retry_orig_test_idx
            WHERE t1.retry_orig_test_idx IS NULL
                  AND t2.retry_orig_test_idx IS NOT NULL
                  AND t1.test_idx >= @starting_idx;
            SET @count = (SELECT ROW_COUNT()) + @count;

        UNTIL @count = 0
        END REPEAT;
    COMMIT;

    CALL log_msg('Finished updating retry info');
END$$
delimiter ;
