# Copyright 2016 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

DROP VIEW IF EXISTS imported_tests_view;
CREATE VIEW imported_tests_view
AS SELECT
import_tko_tests.test_idx,
import_tko_tests.job_idx,
import_jobs.afe_job_id,
test_names.test_name_idx,
import_tko_tests.status AS status_idx,
import_tko_tests.machine_idx,
import_jobs.release_number,
builds.build_idx,
suites.suite_idx,
platforms.platform_idx,
configs.config_idx,
IF(INSTR(import_jobs.`label`, '/experimental'),  TRUE, FALSE) AS is_experimental,
import_jobs.queued_time AS job_queued_time,
# 'repair' tests have NULL as start time but some real time as finished_time
IFNULL(import_tko_tests.started_time, import_tko_tests.finished_time)  AS test_started_time,
import_tko_tests.finished_time AS test_finished_time,
import_tko_tests.reason,
import_jobs.`label` AS job_name,
import_tko_tests.invalid,
import_tko_tests.invalidates_test_idx,
NULL AS retry_orig_test_idx,
import_jobs.fw_rw_version,
import_jobs.fw_ro_version,
import_jobs.test_version
FROM import_tko_tests
JOIN import_tko_machines USING(machine_idx)
JOIN import_jobs USING(job_idx)
JOIN import_tko_status ON(import_tko_tests.status = import_tko_status.status_idx)
JOIN test_names ON (import_tko_tests.test = test_names.test_name)
JOIN suites USING (suite)
JOIN platforms ON (import_tko_machines.machine_group = platforms.platform)
JOIN configs ON (import_jobs.build_config = configs.config)
JOIN builds ON (import_jobs.buildname = builds.build);


DROP VIEW IF EXISTS btests_view;
CREATE VIEW btests_view AS
SELECT *
FROM bad_tests
JOIN suites USING (suite_idx)
JOIN test_names USING (test_name_idx)
JOIN platforms USING (platform_idx)
JOIN configs USING (config_idx)
JOIN builds USING (build_idx);
