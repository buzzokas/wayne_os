# Copyright 2016 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

delimiter $$
DROP PROCEDURE IF EXISTS copy_imported$$
CREATE PROCEDURE copy_imported()
BEGIN
    START TRANSACTION;
        # tko_machines / tko_status tables are small, entirely copied each time.
        TRUNCATE TABLE tko_machines;
        INSERT INTO tko_machines
        SELECT * FROM import_tko_machines order by machine_idx;

        TRUNCATE TABLE tko_status;
        INSERT INTO tko_status
        SELECT * FROM import_tko_status;

        REPLACE INTO autobugs
        SELECT * FROM import_autobugs;

        REPLACE INTO tko_jobs
        SELECT * FROM import_tko_jobs;

        REPLACE INTO tko_job_keyvals
        SELECT * FROM import_tko_job_keyvals;

        REPLACE INTO tko_tests
        SELECT * FROM import_tko_tests;
    COMMIT;
END$$
delimiter ;


delimiter $$
DROP PROCEDURE IF EXISTS clean_import_tables$$
CREATE PROCEDURE clean_import_tables()
BEGIN
    TRUNCATE TABLE import_tko_machines;
    TRUNCATE TABLE import_tko_status;
    TRUNCATE TABLE import_tko_jobs;
    TRUNCATE TABLE import_tko_job_keyvals;
    TRUNCATE TABLE import_tko_tests;
    TRUNCATE TABLE import_autobugs;
END$$
delimiter ;
