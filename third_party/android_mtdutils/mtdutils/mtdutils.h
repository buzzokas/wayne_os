/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MTDUTILS_H_
#define MTDUTILS_H_

#include <stdint.h>
#include <sys/types.h>  // for size_t, etc.

#ifdef __cplusplus
extern "C" {
#endif

typedef struct MtdPartition MtdPartition;

int mtd_scan_partitions(void);

const MtdPartition *mtd_find_partition_by_name(const char *name);

/* mount_point is like "/system"
 * filesystem is like "yaffs2"
 */
int mtd_mount_partition(const MtdPartition *partition, const char *mount_point,
        const char *filesystem, int read_only);

/* get the partition and the minimum erase/write block size.  NULL is ok.
 */
int mtd_partition_info(const MtdPartition *partition,
                       uint64_t *total_size,
                       uint32_t *erase_size,
                       uint32_t *write_size);
/* retrieve information about an MTD dev node, e.g. node_name /dev/mtd12
 */
int mtd_node_info(const char *dev_node_name,
                  uint64_t *total_size,
                  uint32_t *erase_size,
                  uint32_t *write_size);

/* read or write raw data from a partition, starting at the beginning.
 * skips bad blocks as best we can.
 */
typedef struct MtdReadContext MtdReadContext;
typedef struct MtdWriteContext MtdWriteContext;

/* read_partition opens the device node, and owns the file descriptor. */
MtdReadContext *mtd_read_partition(const MtdPartition *);
/* read_descriptor uses the provided file descriptor. */
MtdReadContext *mtd_read_descriptor(int fd, const char *dev_node_name);
ssize_t mtd_read_data(MtdReadContext *, char *data, size_t data_len);
void mtd_read_close(MtdReadContext *);
/* NOTICE: Only use with reads of multiple erase blocks please. */
off64_t mtd_read_skip_to(const MtdReadContext *, off64_t offset);

/* write_partition opens the device node, and owns the file descriptor. */
MtdWriteContext *mtd_write_partition(const MtdPartition *);
/* write_descriptor uses the provided file descriptor. */
MtdWriteContext *mtd_write_descriptor(int fd, const char *dev_node_name);
ssize_t mtd_write_data(MtdWriteContext *, const char *data, size_t data_len);
off_t mtd_erase_blocks(MtdWriteContext *, int blocks);  /* 0 ok, -1 for all */
off_t mtd_find_write_start(MtdWriteContext *ctx, off_t pos);
int mtd_write_close(MtdWriteContext *);

#ifdef __cplusplus
}
#endif

#endif  // MTDUTILS_H_
