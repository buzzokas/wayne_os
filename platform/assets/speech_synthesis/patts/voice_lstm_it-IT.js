// Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// AUTOGENERATED FILE
//
// This file is autogenerated! If you need to modify it, be sure to
// modify the script that exports Google voice data for use in Chrome.

// Initialize the voice array if it doesn't exist so that voice data files
// can be loaded in any order.

if (!window.voices) {
  window.voices = [];
}

// Add this voice to the global voice array.
window.voices.push({
  'pipelineFile': '/voice_lstm_it-IT/kda/pipeline',
  'prefix': '/voice_lstm_it-IT/kda/',
  'voiceType': 'lstm',
  'cacheToDisk': false,
  'lang': 'it-IT',
  'displayName': 'Italian',
  'voiceName': 'Chrome OS italiano',
  'removePaths': [],
  'files': [
    {
      'path': '/voice_lstm_it-IT.zvoice',
      'url': '',
      'md5sum': 'e196173191a9712207edc9fcb668d7e6',
      'size': 8613346,
    },
  ],
});
