# CFM Device Monitor

Source code for CFM peripheral monitors which check the status of CFM devices
and guarantee their liveness.
