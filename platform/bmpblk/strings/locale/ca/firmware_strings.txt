Falta Chrome OS o està malmès.
Insereix una memòria USB o bé una targeta SD de recuperació.
Insereix una memòria USB de recuperació.
Insereix una targeta SD o bé una memòria USB de recuperació (nota: el port USB de color blau NO funcionarà per a la recuperació).
Insereix una memòria USB de recuperació en un dels 4 ports a la part POSTERIOR del dispositiu.
El dispositiu que has inserit no conté Chrome OS.
La verificació del SO està DESACTIVADA
Prem ESPAI per tornar-la a activar.
Prem RETORN per confirmar que vols activar la verificació del SO.
Es reiniciarà el sistema i s'esborraran les dades locals.
Per tornar enrere, prem ESC.
La verificació del SO està ACTIVADA.
Per DESACTIVAR la verificació del SO, prem RETORN.
Per obtenir més informació, visita https://google.com/chromeos/recovery
Codi d'error
Treu tots els dispositius externs per iniciar la recuperació.
Model᠎ 60061e
Per desactivar la verificació del SO, prem el botó RECUPERACIÓ.
La font d'alimentació que has connectat no té prou potència per mantenir el dispositiu encès.
Chrome OS s'apagarà d'aquí a uns instants.
Utilitza l'adaptador correcte i torna-ho a provar.
Treu tots els dispositius connectats i inicia la recuperació.
Prem una tecla numèrica per seleccionar un bootloader alternatiu:
Prem el botó d'engegada per executar un diagnòstic.
Per desactivar la verificació del SO, prem el botó d'engegada.
