export interface RunDiagnosticParams {
    category: string;
    name: string;
}
