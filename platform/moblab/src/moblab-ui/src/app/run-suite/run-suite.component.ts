import {Component} from '@angular/core';

@Component({
  selector: 'app-run-suite',
  template: '<router-outlet></router-outlet>',
  styleUrls: ['../app.component.css'],
})


export class RunSuiteComponent {
}
