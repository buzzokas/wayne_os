# Copyright (c) 2014 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

description     "Setup autotest runtime directory."
author          "chromium-os-dev@chromium.org"

start on ( started cros-disks and
           stopped moblab-external-settings-init )

env EXTERNAL_RESULTS_DIR=/mnt/moblab/results
env ATEST_RESULTS_DIR=/usr/local/autotest/results
env MOUNT_DIR=/mnt/moblab

normal exit 0

script
  mkdir -p /var/log/bootup/
  exec >>/var/log/bootup/${UPSTART_JOB}.log 2>&1
  set -x
  set -e

  echo 0 > /proc/sys/kernel/hung_task_timeout_secs
  # TODO(sbasi) (crbug.com/345138) - Switch to running only after autoupdate
  # rather than every boot.

  # M56 creates a logs directory that needs to be moved out of the way so
  # the simlink to /var/log/autotest can be installed. crbug/709697
  # TODO(haddowk) remove this check when no one could possibly do upgrade from
  # M56
  AUTOTEST_DIR=/usr/local/autotest
  if [ -d "${AUTOTEST_DIR}/logs" ]; then
    mv "${AUTOTEST_DIR}/logs" "${AUTOTEST_DIR}/logs.$(date +%F_%R)"
  fi
  rsync -a /autotest/ "${AUTOTEST_DIR}/"

  if [ -d "/mnt/moblab-settings/site-settings/autotest" ]; then
     /usr/bin/rsync -og --chown=moblab:moblab \
       /mnt/moblab-settings/site-settings/autotest/* "${AUTOTEST_DIR}"
  fi

  # If the shadow_config does no exist, create one.
  if [ ! -s "${AUTOTEST_DIR}"/shadow_config.ini ]; then
    cat > "${AUTOTEST_DIR}"/shadow_config.ini <<EOF
[AUTOTEST_WEB]
host: localhost
password: moblab_db_passwd
readonly_host: localhost
readonly_user: chromeosqa-admin
readonly_password: moblab_db_passwd
wmatrix_url=/wmatrix

[CROS]
dev_server: http://192.168.231.1:8080
devserver_dir: /usr/lib/devserver/

[LUCIFER]
lucifer_level: STARTING

[SERVER]
hostname: localhost

[SCHEDULER]
drones: localhost
drone_build_externals: False

[TKO]
database: chromeos_autotest_db
host: localhost
password: moblab_db_passwd
readonly_host: localhost
readonly_user: chromeosqa-admin
readonly_password: moblab_db_passwd

EOF
  fi

  mkdir -p "${EXTERNAL_RESULTS_DIR}"
  chown -R moblab:moblab "${EXTERNAL_RESULTS_DIR}"
  # If the old results directory exists copy over the files and delete it.
  if [ -e "${ATEST_RESULTS_DIR}" -a ! -L  "${ATEST_RESULTS_DIR}" ]; then
    logger -t "${UPSTART_JOB}" "Copying results directory."
    cp -r "${ATEST_RESULTS_DIR}/" "${MOUNT_DIR}"
    rm -rf "${ATEST_RESULTS_DIR}"
  fi
  # Create the results directory symlink if it does not exist.
  if [ ! -e "${ATEST_RESULTS_DIR}" ]; then
    logger -t "${UPSTART_JOB}" "Creating autotest results dir symlink."
    ln -s "${EXTERNAL_RESULTS_DIR}" "${ATEST_RESULTS_DIR}"
  fi

  chown -R moblab:moblab "${MOUNT_DIR}/results" || :

  # Make the logs directory and give ownership to moblab.
  mkdir -p /var/log/autotest
  chown -R moblab:moblab /var/log/autotest
  chmod 660 "${AUTOTEST_DIR}"/shadow_config.ini
  # Give moblab ownership of the autotest directory and make sure it has
  # rights to execute the scripts.
  chown -R moblab:moblab "${AUTOTEST_DIR}"
  # The autotest files try to exec all sorts of scripts.  The ebuild only
  # installs them with 644 perms because historically the servers have just
  # clobbered all the perms to +x anyways.  Until we clean up the autotest
  # ebuild to install files with the right perms, we have to keep this hack.
  find "${AUTOTEST_DIR}" -type f ! -perm /1 -exec chmod a+x {} + || :
end script
