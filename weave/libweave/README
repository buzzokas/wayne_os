Overview
--------
libWeave is the library to with device side implementation of Weave
protocol.

Sources
-------
Sources are located in git repository at
https://weave.googlesource.com/weave/libweave/


Install Repo
-------
1. Make sure you have a bin/ directory in your home directory
and that it is included in your path:

    mkdir ~/bin
    PATH=~/bin:$PATH

2. Download the Repo tool and ensure that it is executable:

    curl https://storage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
    chmod a+x ~/bin/repo

Checkout code
-------
    repo init -u https://weave.googlesource.com/weave/manifest
    repo sync

Directory structure
-------------------
Includes to be used by device code:
  include/

Implementation sources:
  src/

Example of device code:
  examples/

Dependencies:
  third_party/

Build files:
  libweave.gyp

Quick start on Debian/Ubuntu
----------------------------

Install prerequisites:

  examples/prerequisites.sh

Build library, tests, run tests, build example:

  examples/build.sh

Execute example (check examples/daemon/README for details):

  sudo out/Debug/weave_daemon


Prerequisites
-------------
Common:

  autoconf
  automake
  binutils
  libtool
  gyp
  libexpat1-dev
  ninja-build

For tests:

  gtest
  gmock

For examples:

  hostapd
  libavahi-client-dev
  libcurl4-openssl-dev
  libevent 2.1.x-alpha


Compiling
---------
Everywhere below Debug can be replaced with Release.

Generate ninja build files:

  gyp --toplevel-dir=. --depth=. -f ninja libweave.gyp

Build library with tests:

  ninja -C out/Debug

Build library only:

  ninja -C out/Debug libweave

Testing
-------
Run unittests tests:

  out/Debug/libweave_testrunner
  out/Debug/libweave_exports_testrunner

Making changes
--------------
Make sure to have correct user in local or global config e.g.:

  git config --local user.name "John Doe"
  git config --local user.email johndoe@example.com

Start local branch

  repo start <branch name> .

Edit code and commit locally e.g.:

  git commit -a -v

Upload CL:

  repo upload .

Go to the url from the output of "repo upload" and add reviewers.

Known Issues
------------
* No big-endian support. Pairing fails on big-endian hardware.
