#
# Copyright 2018 The Chromium OS Authors. All rights reserved.
#
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

audio-common: &audio_common
  $cras-root: "/etc/cras"
  $ucm-root: "/usr/share/alsa/ucm"
  $card: "bxtda7219max"
  # Both source/target path for cras files.
  # Generally set to device name or 'common'
  $cras: "{{$device-name}}"
  $ucm: "1mic" # Default
  $ucm-device: "common"
  $ucm-target-suffix: ""
  $ucm-suffix-path: "/{{$ucm}}"
  cras-config-dir: "{{$cras}}"
  ucm-suffix: "{{$ucm}}{{$ucm-target-suffix}}"
  $cras-source: "{{cras-config-dir}}/audio/cras-config"
  $cras-dest: "{{$cras-root}}/{{cras-config-dir}}"
  $ucm-source: "{{$ucm-device}}/audio/ucm-config{{$ucm-suffix-path}}"
  $ucm-dest: "{{$ucm-root}}/{{$card}}.{{ucm-suffix}}"
  files:
    - source: "{{$cras-source}}/{{$card}}"
      destination: "{{$cras-dest}}/{{$card}}"
    - source: "{{$cras-source}}/dsp.ini"
      destination: "{{$cras-dest}}/dsp.ini"
    - source: "{{$ucm-source}}/HiFi"
      destination: "{{$ucm-dest}}/HiFi"
    - source: "{{$ucm-source}}/{{$card}}.conf"
      destination: "{{$ucm-dest}}/{{$card}}.{{ucm-suffix}}.conf"

device-config: &device_config
  $bluetooth-device: "common"
  $dptf-device: "{{$device-name}}"
  $touchpad-wakeup: "1"
  $test-label: "{{$device-name}}"
  name: "{{$device-name}}"
  audio:
    main: *audio_common
  bluetooth:
    config:
      build-path: "{{$bluetooth-device}}/bluetooth/main.conf"
      system-path: "/etc/bluetooth/{{$bluetooth-device}}/main.conf"
  firmware:
    no-firmware: True
  test-label: "{{$test-label}}"
  power:
    charging-ports: |
      CROS_USB_PD_CHARGER0 LEFT
      CROS_USB_PD_CHARGER1 RIGHT
    low-battery-shutdown-percent: "4.0"
    power-supply-full-factor: "0.94"
    suspend-to-idle: "1"
    touchpad-wakeup: "{{$touchpad-wakeup}}"
  thermal:
    dptf-dv: "{{$dptf-device}}/dptf.dv"
    files:
      - source: "{{$dptf-device}}/thermal/dptf.dv"
        destination: "/etc/dptf/{{$dptf-device}}/dptf.dv"
  identity: &base_ident
    platform-name: "Coral"
    smbios-name-match: "Coral"
    sku-id: "{{$sku-id}}"

convertible-base-config: &convertible_base_config
  hardware-properties:
    is-lid-convertible: True
    has-lid-accelerometer: True
    has-base-accelerometer: True
    has-base-gyroscope: True

convertible-config: &convertible_config
  <<: [ *device_config, *convertible_base_config ]

wl-device-config: &wl_device_config
  <<: *device_config
  identity:
    <<: *base_ident
    whitelabel-tag: "{{$whitelabel-tag}}"

wl-convertible-config: &wl_convertible_config
  <<: [ *wl_device_config, *convertible_base_config ]

astronaut-config: &astronaut_config
  <<: *device_config
  modem:
    firmware-variant: "{{$device-name}}"

chromeos:
  devices:
    - $device-name: "astronaut"
      skus:
        - $sku-id: 0
          config: *astronaut_config
        - $sku-id: 1
          config: *astronaut_config
        - $sku-id: 61
          config: *astronaut_config
        - $sku-id: 62
          config: *astronaut_config
    - $device-name: "babymega"
      $ucm-device: "babymega"
      $ucm-target-suffix: "Babymega"
      skus:
        - $sku-id: 52
          config: *device_config
        - $sku-id: 53
          config: *device_config
    - $device-name: "babytiger"
      $ucm-device: "babytiger"
      $ucm-target-suffix: "Babytiger"
      $ucm: "2mic"
      skus:
        - $sku-id: 30
          config: *device_config
        - $sku-id: 33
          config: *device_config
    - $device-name: "blacktip"
      $bluetooth-device: "blacktip"
      $dptf-device: "astronaut"
      products:
        - $whitelabel-tag: "" # default
        - $whitelabel-tag: "ctl"
        - $whitelabel-tag: "gsa"
        - $whitelabel-tag: "positivo"
        - $whitelabel-tag: "lanix"
        # Workaround to make it compatible with "ctl"
        - $whitelabel-tag: "to-be-determined-bt-loem1"
      skus:
        - $sku-id: 36
          config: *wl_device_config
    - $device-name: "blacktip360"
      $bluetooth-device: "blacktip"
      $ucm: "2mic"
      $touchpad-wakeup: "0"
      $dptf-device: "astronaut"
      products:
        - $whitelabel-tag: "" # default
        - $whitelabel-tag: "ctl"
        - $whitelabel-tag: "gsa"
        - $whitelabel-tag: "xma"
        - $whitelabel-tag: "positivo"
        - $whitelabel-tag: "lanix"
        # Workaround to make it compatible with "ctl"
        - $whitelabel-tag: "to-be-determined-bt-loem1"
      skus:
        - $sku-id: 37
          config: *wl_convertible_config
        - $sku-id: 38
          config: *wl_convertible_config
    - $device-name: "blacktiplte"
      $bluetooth-device: "blacktip"
      $dptf-device: "astronaut"
      products:
        - $whitelabel-tag: "default" # default
        - $whitelabel-tag: "ctl"
        - $whitelabel-tag: "gsa"
      skus:
        - $sku-id: 65
          config:
            <<: *wl_device_config
            modem:
              firmware-variant: "{{$device-name}}"
    - $device-name: "blue"
      $dptf-device: "astronaut"
      skus:
        - $sku-id: 6
          config: *device_config
        - $sku-id: 7
          config: *device_config
        - $sku-id: 12
          config: *device_config
    - $device-name: "bruce"
      $touchpad-wakeup: "0"
      $dptf-device: "common"
      skus:
        - $sku-id: 8
          config: *convertible_config
        - $sku-id: 11
          $ucm: "2mic"
          $ucm-device: "bruce"
          $ucm-target-suffix: "Bruce360"
          config: *convertible_config
    - $device-name: "epaulette"
      $dptf-device: "astronaut"
      # TODO(epaulette-odm): Modify the current conf files as
      # required, they are copies from other 2-mic model.
      $ucm: "2mic"
      $ucm-device: "epaulette"
      $ucm-target-suffix: "Epaulette"
      # TODO(epaulette-odm): Add validated dptf.dv file into
      # BSP, it's currently using inherited version from the parent.
      skus:
        - $sku-id: 13
          config: *device_config
        - $sku-id: 14
          config: *device_config
        - $sku-id: 15
          config: *device_config
        - $sku-id: 16
          config: *device_config
    - $device-name: "lava"
      $touchpad-wakeup: "0"
      $dptf-device: "astronaut"
      skus:
        - $sku-id: 4
          config: *convertible_config
        - $sku-id: 5
          config: *convertible_config
        - $sku-id: 9
          config: *convertible_config
        - $sku-id: 10
          config: *convertible_config
    - $device-name: "nasher"
      $ucm: ""
      $ucm-suffix-path: ""
      $ucm-device: "nasher"
      $ucm-target-suffix: "nasher"
      skus:
        - $sku-id: 160
          config: *device_config
        - $sku-id: 161
          config: *device_config
        - $sku-id: 162
          config: *device_config
    - $device-name: "nasher360"
      $cras: "nasher"
      $ucm-device: "nasher360"
      $ucm: "2mic"
      $ucm-target-suffix: "Nasher360"
      $touchpad-wakeup: "0"
      $dptf-device: "nasher"
      skus:
        - $sku-id: 163
          $ucm: "nasher"
          config: *convertible_config
        - $sku-id: 164
          $ucm: "nasher"
          config: *convertible_config
        - $sku-id: 165
          $ucm: "2micNasher360"
          config: *convertible_config
        - $sku-id: 166
          $ucm: "2micNasher360"
          config: *convertible_config
    - $device-name: "porbeagle"
      $dptf-device: "astronaut"
      $cras: "astronaut"
      skus:
        - $sku-id: 26
          config: *device_config
        - $sku-id: 27
          config: *device_config
    - $device-name: "rabbid"
      skus:
        - $sku-id: 28
          config:
            <<: *device_config
            $cras: "rabbid_rugged"
            $ucm-device: "rabbid_rugged"
            $ucm: "2mic"
            $ucm-target-suffix: "Rabbid_rugged"
        - $sku-id: 31
          config:
            <<: *device_config
            $ucm-device: "rabbid"
            $ucm: "2mic"
            $ucm-target-suffix: "Rabbid"
        - $sku-id: 32
          config:
            <<: *device_config
            $ucm-device: "rabbid"
            $ucm: "2mic"
            $ucm-target-suffix: "Rabbid"
    - $device-name: "robo"
      skus:
        - $sku-id: 70
          config: *device_config
    - $device-name: "robo360"
      $cras: "nasher"
      $ucm: ""
      $ucm-suffix-path: ""
      $ucm-device: "robo360"
      $ucm-target-suffix: "robo360"
      $dptf-device: "robo"
      $touchpad-wakeup: "0"
      skus:
        - $sku-id: 71
          config: *convertible_config
    - $device-name: "santa"
      $cras: "astronaut"
      $dptf-device: "astronaut"
      skus:
        - $sku-id: 2
          config: *device_config
        - $sku-id: 3
          config: *device_config
    - $device-name: "whitetip"
      $test-label: "robo"
      products:
        - $whitelabel-tag: "" # default
        - $whitelabel-tag: "ctl"
        - $whitelabel-tag: "prowise"
        - $whitelabel-tag: "ytl"
        - $whitelabel-tag: "sector5"
        - $whitelabel-tag: "pcmerge"
        - $whitelabel-tag: "xma"
        - $whitelabel-tag: "multilaser"
      skus:
        - $sku-id: 78
          config: *wl_device_config
        - $sku-id: 82
          config: *wl_device_config
